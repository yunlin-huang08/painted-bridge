package com.tripreset.app.painted.bridge.home

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import com.tripreset.app.painted.bridge.components.LoadImage
import com.tripreset.app.painted.bridge.ui.theme.PaintedBridgeTheme
import com.tripreset.app.painted.bridge.ui.theme.Teal200CC
import com.tripreset.app.painted.bridge.ui.theme.Teal200CC2

@Composable
fun HomeScreenWithImageList(urlList: List<String>) {
    LazyColumn {
        items(urlList) { url ->
            HomeListCell(url)
        }
    }
}

@Composable
fun HomeListCell(url: String) {

    Box(
        modifier = Modifier
            .fillMaxWidth()
            .fillMaxHeight()
    ) {
        url.LoadImage()
        Box(
            modifier = Modifier
                .matchParentSize(),
            contentAlignment = Alignment.BottomCenter
        ) {

            Text(
                text = "111111",
                Modifier
                    .fillMaxWidth()
                    .align(Alignment.BottomCenter)
                    .background(Teal200CC)
                    .padding(16.dp),
                textAlign = TextAlign.Center,
                color = Color.White
            )

            Text(
                text = "",
                Modifier
                    .fillMaxWidth()
                    .align(Alignment.TopCenter)
                    .background(Teal200CC2)
                    .padding(16.dp),
                textAlign = TextAlign.Center,
                color = Color.White
            )
        }

    }

}

@Composable
fun HomeScreen(helloViewModel: HomeViewModel = viewModel()) {
    PaintedBridgeTheme {
        // A surface container using the 'background' color from the theme
        Surface(color = MaterialTheme.colors.background) {
            Column {
                HomeScreenWithImageList(helloViewModel.urlList)
            }
        }
    }
}

@Composable
fun Greeting(name: String, backgroundColor: Brush) {
    Text(
        text = if (name.isEmpty()) "" else "Hello $name!",
        Modifier
            .fillMaxWidth()
            .background(backgroundColor)
            .padding(16.dp),
        textAlign = TextAlign.Center,
        color = Color.White
    )
}

@Preview(showBackground = true)
@Composable
fun DefaultPreview() {
    PaintedBridgeTheme {
        HomeScreen()
    }
}