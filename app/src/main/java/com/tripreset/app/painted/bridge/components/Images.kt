package com.tripreset.app.painted.bridge.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.stringResource
import com.google.accompanist.coil.rememberCoilPainter
import com.tripreset.app.painted.bridge.R

@Composable
fun String.LoadImage() = Image(
    painter = rememberCoilPainter(this, fadeIn = true),
    contentDescription = stringResource(R.string.image_content_desc),
    contentScale = ContentScale.Crop,
    modifier = Modifier.fillMaxWidth()
)
