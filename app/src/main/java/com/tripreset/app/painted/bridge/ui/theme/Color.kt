package com.tripreset.app.painted.bridge.ui.theme

import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.core.graphics.toColorInt

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)
val Teal200CCCC by lazy { Color("#00000000".toColorInt()) }
val Teal20066 by lazy { Color("#21000000".toColorInt()) }
val Teal2007F by lazy { Color("#7F000000".toColorInt()) }
val Teal200CC by lazy {
    Brush.verticalGradient(
        0.0f to Teal200CCCC,
        0.3f to Teal20066,
        1.0f to Teal2007F,
    )
}

val Teal200CC2 by lazy {
    Brush.verticalGradient(
        0.0f to Teal200CCCC,
        0.3f to Teal20066,
        1.0f to Teal2007F,
        startY = Float.POSITIVE_INFINITY,
        endY = 0f,
    )
}